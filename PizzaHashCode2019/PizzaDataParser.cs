﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaHashCode2019
{
    static class PizzaDataParser
    {
        private readonly static string DataFolderName = "Data";

        public static Pizza Parse(string fileName)
        {
            using (StreamReader sr = new StreamReader($"{DataFolderName}\\{fileName}"))
            {
                Header header = new Header(sr.ReadLine());

                Pizza pizza = new Pizza(
                    header.NumberOfRows,
                    header.NumberOfColumns,
                    header.MinimalNumberOfIngredients,
                    header.MaximalSizeOfSlice);

                int row = 0;                

                while (!sr.EndOfStream)
                {
                    IList<Ingredient> ingredients = new IngredientsLine(sr.ReadLine()).Ingredients;

                    int column = 0;

                    foreach (var ingredient in ingredients)
                    {
                        pizza.Add(new Cell(ingredient), new Position (row, column));
                        column++;
                    }

                    row++;
                }

                return pizza;
            }
        }        
    }
}
