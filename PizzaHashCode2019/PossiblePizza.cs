﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaHashCode2019
{
    public static class PossiblePizza
    {
        public static readonly string Example = "a_example.in";
        public static readonly string Small = "b_small.in";
        public static readonly string Medium = "c_medium.in";
        public static readonly string Big = "d_big.in";
    }
}
