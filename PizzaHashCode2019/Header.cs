﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaHashCode2019
{
    public class Header
    {
        private const char LineDivider = ' ';
                
        private const int RowIndex = 0;
        private const int ColumnIndex = 1;
        private const int MinIndex = 2;
        private const int MaxIndex = 3;

        public int NumberOfRows { get; private set; }

        public int NumberOfColumns { get; private set; }

        public int MinimalNumberOfIngredients { get; private set; }

        public int MaximalSizeOfSlice { get; private set; }

        public Header(string header)
        {
            var headerParts = header.Split(LineDivider);

            NumberOfRows = int.Parse(headerParts[RowIndex]);
            NumberOfColumns = int.Parse(headerParts[ColumnIndex]);
            MinimalNumberOfIngredients = int.Parse(headerParts[MinIndex]);
            MaximalSizeOfSlice = int.Parse(headerParts[MaxIndex]);
        }
    }
}
