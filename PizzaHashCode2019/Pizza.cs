﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaHashCode2019
{
    public class Pizza
    {
        private readonly Cell[,] contents;

        public int NumberOfRows { get; private set; }

        public int NumberOfColumns { get; private set; }

        public int MinimalNumberOfIngredients { get; private set; }

        public int MaximalSizeOfSlice { get; private set; }

        public Pizza(int numberOfRows, int numberOfColumns, int minimalNumberOfIngredients, int maximalSizeOfSlice)
        {
            NumberOfRows = numberOfRows;
            NumberOfColumns = numberOfColumns;
            MinimalNumberOfIngredients = minimalNumberOfIngredients;
            MaximalSizeOfSlice = maximalSizeOfSlice;

            contents = new Cell[numberOfRows, numberOfColumns];
        }

        public void Add(Cell cell, Position position)
        {
            contents[position.Row, position.Column] = cell;
        }
    }
}
