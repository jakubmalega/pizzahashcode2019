﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaHashCode2019
{
    class Program
    {
        static void Main(string[] args)
        {
            var examplePizza = PizzaDataParser.Parse(PossiblePizza.Example);
            var smallPizza = PizzaDataParser.Parse(PossiblePizza.Small);
            var mediumPizza = PizzaDataParser.Parse(PossiblePizza.Medium);            
            var bigPizza = PizzaDataParser.Parse(PossiblePizza.Big);
        }
    }
}