﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaHashCode2019
{
    public class Cell
    {
        public Ingredient Ingredient { get; private set; }

        public Cell(Ingredient ingredient)
        {
            Ingredient = ingredient;
        }
    }
}
