﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaHashCode2019
{
    public class IngredientsLine
    {
        private const char LineDivider = ' ';
        private const char TomatoeChar = 'T';
        private const char MushRoomChar = 'M';

        public IList<Ingredient> Ingredients { get; private set; }

        public IngredientsLine(string line)
        {
            Ingredients = new List<Ingredient>();

            foreach(var ingredientChar in line)
            {
                if(ingredientChar == TomatoeChar)
                {
                    Ingredients.Add(Ingredient.Tomato);
                }

                if(ingredientChar == MushRoomChar)
                {
                    Ingredients.Add(Ingredient.Mushroom);
                }
            }
        }
    }
}
